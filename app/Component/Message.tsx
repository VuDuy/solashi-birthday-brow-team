import React from "react";
import CustomImage from "./CustomImage";

export default function Message() {
  const randomNumber = () => {
    const randomNumber = Math.random();

  if (randomNumber < 0.5) {
    return 1;
  } else {
    return 2;
  }
  }
  return (<div style={{backgroundColor: 'white', borderRadius: '14px', width: '100%', minHeight: '200px', padding: '10px 20px', display: 'flex'}}>
    <div style={{width: '30%', borderRight: '2px solid #f3f3f3', display: 'flex', alignItems: 'flex-end'}}>
      <CustomImage src="/Images/cake.png" width="200px" height="200px" alt="cake" />
    </div>
    <div style={{width: '70%',position: 'relative'}}>
      <div style={{ position: 'absolute', bottom: '-10px', right: '-10px'}}>
        <CustomImage src={"/Images/card-" + randomNumber() + ".png"} width="100px" height="auto" alt="bottom" />
      </div>
    </div>
  </div>)
}