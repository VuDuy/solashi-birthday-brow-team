import React, { useEffect } from "react";
import * as THREE from "three";

import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js";

export default function NumberFive() {
  const fileURL = '/Images/number-5.glb'
  useEffect(() => {
    const canvas: HTMLCanvasElement | null =
      typeof document !== "undefined"
        ? document.querySelector("canvas.webgl")
        : null;
    const containParent: HTMLElement | null =
      document.getElementById("contain");
    const scene = new THREE.Scene();
    let renderer: any;
    let camera: THREE.PerspectiveCamera;

    const width = containParent ? containParent.offsetWidth : 400;
    const height = containParent ? containParent.offsetHeight : 400;
    if (fileURL) {
      init(); //our setup
      render(); //the update loop
    }

    function init() {
      //setup the camera
      camera = new THREE.PerspectiveCamera(15, width / height, 0.25, 20);
      camera.position.set(-1.8, 0.6, 2.7);

      // load the model
      const loader = new GLTFLoader();
      loader.load(fileURL, function (gltf) {
        scene.add(gltf.scene);
        render(); //render the scene for the first time
        function animate() {
          requestAnimationFrame(animate);
          gltf.scene.rotation.y += -0.02;  
          renderer.render(scene, camera);
        }
        
        animate();
      });

      //setup the renderer
      renderer = new THREE.WebGLRenderer({
        antialias: true,
        canvas: canvas as HTMLCanvasElement,
        alpha: true,
      });
      renderer.setSize(width, height);
      // renderer.outputEncoding = THREE.sRGBEncoding; //extended color space for the hdr

      const controls = new OrbitControls(camera, renderer.domElement);
      controls.addEventListener("change", render); // use if there is no animation loop to render after any changes
      controls.minDistance = 2;
      controls.maxDistance = 10;
      controls.target.set(0, 0, 0);
      controls.update();

      window.addEventListener("resize", onWindowResize);
    }

    function onWindowResize() {
      camera.aspect = width / height;
      camera.updateProjectionMatrix();

      renderer.setSize(width, height);

      render();
    }

    //

    function render() {
      renderer.render(scene, camera);
    }
  }, [fileURL]);

  return (
    <canvas className="webgl"></canvas>
  )
}