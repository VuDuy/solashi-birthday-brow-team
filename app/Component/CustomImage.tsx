import React, { ImgHTMLAttributes } from "react";

interface Props extends ImgHTMLAttributes<HTMLImageElement> {
  src: string;
  width?: string;
  height?: string;
}

const CustomImage: React.FC<Props> = ({ src, width, height, ...props }) => {
  return <img src={src} width={width} height={height} {...props} />;
};

export default CustomImage;
