import React from "react";

export default function VideoShow() {
  return (
    <video src="/Videos/Test.mp4" width={'100%'} height={'100%'} controls loop>
      <source src="/Videos/Test.mp4" type="video/mp4" />
    </video>
  )
}