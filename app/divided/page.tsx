'use client'
import CustomImage from "../Component/CustomImage";
import Message from "../Component/Message";
import NumberFive from "../Component/NumberFive";
import VideoShow from "../Component/VideoShow";

export default function Home() {
  return (
    <main style={{overflowX: 'hidden'}}>
      <div className="background">
        <div style={{ width: '100%', height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <NumberFive />
        </div>
        <div style={{ position: 'absolute', top: '5%', left: '5%', transform: 'rotate(-20deg)' }}>
          <CustomImage src="/Images/solashi.png" width="750px" height="250px" alt="solashi" />
        </div>
      </div>
      <div style={{padding: '100px 20px', backgroundColor: '#0f0f11'}}>
        <VideoShow />
      </div>

      {/* <div style={{padding: '100px 20px', backgroundColor: '#0f0f11'}}>
        <div style={{display: 'flex', gap: '20px', flexWrap: 'wrap'}}>
        <Message />
        <Message />
        <Message />
        </div>
      </div> */}
      <div className="snowflakes" aria-hidden="true" style={{ zIndex: 100 }}>
          <div className="snowflake">
            <CustomImage src="/Images/ssss.png" alt="" width="20px" height="20px" />
          </div>
          <div className="snowflake">
            <CustomImage src="/Images/ssss.png" alt="" width="20px" height="20px" />
          </div>
          <div className="snowflake">
            <CustomImage src="/Images/ssss.png" alt="" width="20px" height="20px" />
          </div>
          <div className="snowflake">
            <CustomImage src="/Images/ssss.png" alt="" width="20px" height="20px" />
          </div>
          <div className="snowflake">
            <CustomImage src="/Images/ssss.png" alt="" width="20px" height="20px" />
          </div>
          <div className="snowflake">
            <CustomImage src="/Images/ssss.png" alt="" width="20px" height="20px" />
          </div>
          <div className="snowflake">
            <CustomImage src="/Images/ssss.png" alt="" width="20px" height="20px" />
          </div>
          <div className="snowflake">
            <CustomImage src="/Images/ssss.png" alt="" width="20px" height="20px" />
          </div>
          <div className="snowflake">
            <CustomImage src="/Images/ssss.png" alt="" width="20px" height="20px" />
          </div>
        </div>
    </main>
  );
}
